const express = require('express');
const bodyParser = require('body-parser');
const User = require('./models/users');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const session = require('express-session');

mongoose.connect('mongodb://mean:25051989b@ds141221.mlab.com:41221/mean', {
            useNewUrlParser: true
    }).then(() => {
    console.log('Database connected');
}).catch((error) => {
    console.log(error);
    console.log('Connection failure');
});


const app = express();

app.use(cookieParser());

app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 10
    }
}));

app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));
app.use(bodyParser.json());

// middleware function to check for logged-in users
const sessionChecker = (req, res, next) => {
    console.log(req.session.user);
    if (req.session.user && req.body.user_sid) {
       next();
    } else {
       res.status(400).json({
           message: "valid key not send"
       });
    }
};

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin,X-Requested-With,Content-Type,Accept');
    res.setHeader('Access-Control-Allow-Methods', 'PUT,POST,DELETE,GET,OPTIONS,PATCH');
    next();
});



// route for user signup
app.post('/api/signup', (req,res,next) => { 
    const user = new User({
        email: req.body.email,
        password: req.body.password,
        name: null
    });
    console.log(user);
    user.save().then((result) => {
        req.session.user = result.dataValues;
        res.status(201).json({
            message: 'User Registered sucessfully',
        });
    });



});
module.exports = app;
    

app.post('/api/login', (req, res, next) => {
    User.findOne({email: req.body.email, password: req.body.password}).then((result) => {
        console.log(result);
        req.session.user = result.dataValues;
        res.status(201).json({
            message: 'User LoggedIn sucessfully',
            user_sid: result._id
        });
    }).catch((error) => {
        res.status(400).json({
            message: "User Can't find",

        });
    });

});

app.put('/api/users', (sessionChecker),(req, res, next) => {
    const user = new User({
        _id: req.body.user_sid,
        name: req.body.name,
        password: req.body.password
    });
    User.updateOne({
        _id: req.body.user_sid
    }, user).then((result) => {
        console.log(result);
        res.status(200).json({
            message: 'updated successfully',
            user_sid: req.body.user_sid
        });
    }).catch((error) => {
        res.status(400).json({
            message: 'Please provide valid credentials'
        });
    });

});
